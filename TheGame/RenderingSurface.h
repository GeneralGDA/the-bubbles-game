#pragma once

#include <windows.h>
#include <wingdi.h>
#include <windef.h>

#include "BaseException.h"

namespace utils
{
	class RenderingSurface
	{
	private:

		const HDC m_hdc;
		HGLRC m_renderingContext;

		RenderingSurface(const RenderingSurface&);
		RenderingSurface& operator=(const RenderingSurface&);

	public:

		explicit RenderingSurface(const HWND windowHandle);
		~RenderingSurface();

		HDC getHDC() const;

		void beginFrame();
		void endFrame();

	};

	class SurfaceCreationException : public BaseException	
	{
	public:

		explicit SurfaceCreationException(const TCHAR* const what);

	};

}