#include <iostream>
#include <windows.h>
#include <gl\GL.h>

#include "RenderingSurface.h"

namespace utils
{
	RenderingSurface::RenderingSurface(const HWND windowHandle)
		:
		m_hdc(::GetWindowDC(windowHandle))
	{
		//discribe requesting pixel format
		int pixelFormat = 0;
		PIXELFORMATDESCRIPTOR pfd;

		::memset(&pfd, 0, sizeof(PIXELFORMATDESCRIPTOR));
		pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
		pfd.nVersion = 1;    
		pfd.dwFlags = PFD_DOUBLEBUFFER | PFD_SUPPORT_OPENGL | PFD_DRAW_TO_WINDOW;
		pfd.iPixelType = PFD_TYPE_RGBA;
		pfd.cColorBits = 24;
		//in 2D game there is no need id
		pfd.cDepthBits = 0;
		pfd.iLayerType = PFD_MAIN_PLANE;

		//call for pixel format
		pixelFormat = ::ChoosePixelFormat(m_hdc, &pfd);
		
		if (0 >= pixelFormat)
		{
			throw SurfaceCreationException(L"Failed to choose pixel format");
		}

		//setup pixel format for our window
		if (FALSE == ::SetPixelFormat(m_hdc, pixelFormat, &pfd))
		{
			throw SurfaceCreationException(L"Failed to set pixel");
		}

		//creating rendering-context
		m_renderingContext = ::wglCreateContext(m_hdc);
		
		if (0 == m_renderingContext)
		{
			throw SurfaceCreationException(L"Failed to create OpenGL context");
		}

		::wglMakeCurrent(m_hdc, m_renderingContext);
		
		glClearColor(0, 0, 0, 1);
	}

	void RenderingSurface::beginFrame()
	{
		glClear(GL_COLOR_BUFFER_BIT);
	}

	void RenderingSurface::endFrame()
	{
		SwapBuffers(m_hdc);
	}

	HDC RenderingSurface::getHDC() const
	{
		return m_hdc;
	}

	RenderingSurface::~RenderingSurface()
	{
		if (FALSE == ::wglDeleteContext(m_renderingContext))
		{
			std::cerr << "Failed to delete OpenGL context" << std::endl;
		}
	}

	SurfaceCreationException::SurfaceCreationException(const TCHAR* const what)
		:
		BaseException(what)
	{
	}

}
