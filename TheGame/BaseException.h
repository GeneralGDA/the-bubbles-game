#pragma once

#include <tchar.h>

namespace utils
{

	class BaseException
	{
	private:

		const TCHAR* m_what;

	public:

		explicit BaseException(const TCHAR* const what);
		virtual ~BaseException() {}

		const TCHAR* const what() const;

	};

}
