#pragma once

#include <vector>
#include <windef.h>

#include "Bubble.h"

namespace game
{
	namespace render
	{
		class BubblesRender
		{
		private:

			static const int BUBBLE_VERTICES_COUNT = 32;
			
			static const int COLOR_CHANNELS = 3;
			static const int POSITION_COMPONENTS = 2;

			//"*2" - to components per vertex: x and y
			float m_bubbleVertices[BUBBLE_VERTICES_COUNT * POSITION_COMPONENTS];

			float m_bubbleVerticesColors[BUBBLE_VERTICES_COUNT * COLOR_CHANNELS];

			int m_viewWidth;
			int m_viewHeight;

			static const int SCORES_LABEL_MAX_TEXT_LENGTH = 256;
			char scoresLabelText[SCORES_LABEL_MAX_TEXT_LENGTH];

		public:

			BubblesRender(const HDC hdc);

			void prepareView(const int viewWidth, const int viewHeight);
			void perform(const std::vector<Bubble>& bubbles, const int scores);

		};
	}
}
