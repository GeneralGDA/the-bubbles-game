#include "BaseException.h"

namespace utils
{
	BaseException::BaseException(const TCHAR* const what)
		:
		m_what(what)
	{
	}

	const TCHAR* const BaseException::what() const
	{
		return m_what;
	}
}