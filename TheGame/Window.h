#pragma once

#include <tchar.h>
#include <windows.h>

#include "BaseException.h"

namespace utils
{
	
	class Window
	{
	private:

		static const int MAX_RESOURCE_STRING_LENGTH = 100;

		TCHAR m_title[MAX_RESOURCE_STRING_LENGTH];
		TCHAR m_windowClass[MAX_RESOURCE_STRING_LENGTH];

		void RegisterClass(WNDPROC windowProcedure);
		void CreateMyWindow(int width, int height);

		HWND m_handle;
		HINSTANCE m_hInstance;

		Window(const Window&);
		Window& operator=(const Window&);

	public:

		Window(const int width, const int height, WNDPROC windowProcedure, HINSTANCE hInstance, int nCmdShow);
		~Window();

		HWND getHandle() const;

	};

	class WindowCreationException : public BaseException
	{
	public:

		explicit WindowCreationException(const TCHAR* const what);

	};

}

