#define _USE_MATH_DEFINES
#include <cmath>
#include <cassert>
#include <windows.h>
#include <gl\GL.h>

#include "BubblesRender.h"

namespace game
{
	namespace render
	{
		namespace
		{
			const int X = 0;
			const int Y = 1;

			const int R = 0;
			const int G = 1;
			const int B = 2;

			const int BASE_TEXT_LIST_INDEX = 2000;

			const GLfloat SCORES_LEFT_OFFSET = 5;
			const GLfloat SCORES_BOTTOM_OFFSET = 5;

			const int SCORES_MAX_TEXT_LENGTH = 1024;
		}

		BubblesRender::BubblesRender(const HDC hdc)
			:
			m_viewWidth(0),
			m_viewHeight(0)
		{
			//generating bubble geometry protototype: identity circle in the coords system center

			float currentAngle = 0.0f;
			const float angleStep = static_cast<float>((M_PI * 2.0f) / BUBBLE_VERTICES_COUNT);

			for (int i = 0; i < BUBBLE_VERTICES_COUNT; ++i)
			{
				m_bubbleVertices[i * POSITION_COMPONENTS + X] = ::cos(currentAngle);
				m_bubbleVertices[i * POSITION_COMPONENTS + Y] = ::sin(currentAngle);
				currentAngle += angleStep;
			}

			//totally unportable (works only on windows) way of text rendering: I'm too lazy to implement font as a texture
			::SelectObject(hdc, ::GetStockObject(SYSTEM_FONT));
			::wglUseFontBitmaps(hdc, 0, 255, BASE_TEXT_LIST_INDEX);
		}

		void BubblesRender::prepareView(const int viewWidth, const int viewHeight)
		{
			assert(0 < viewWidth && "\'viewWidth\' argument must be greater than zero");
			assert(0 < viewHeight && "\'viewHeight\' argument must be greater than zero");

			m_viewWidth = viewWidth;
			m_viewHeight = viewHeight;

			glViewport(0, 0, viewWidth, viewHeight);
			
			glMatrixMode(GL_PROJECTION);
			glLoadIdentity();
			glOrtho(0.0f, viewWidth, viewHeight, 0.0f, -1.0f, 1.0f);
			
			glMatrixMode(GL_MODELVIEW);
			glLoadIdentity();

			glDisable(GL_DEPTH_TEST);
			glDisable(GL_CULL_FACE);
		}

		void BubblesRender::perform(const std::vector<Bubble>& bubbles, const int scores)
		{
			//VBOs from OpenGL 2.0 should improve perfomance
			glEnableClientState(GL_VERTEX_ARRAY);
			glEnableClientState(GL_COLOR_ARRAY);
			glVertexPointer(POSITION_COMPONENTS, GL_FLOAT, 0, m_bubbleVertices);

			for (std::vector<Bubble>::const_iterator i = bubbles.begin(), end = bubbles.end(); i != end; ++i)
			{
				/*
				glColorX is not portable (absent in OpenGL ES), so fill color array for each bubble.
				Another way - use shaders and pass color as uniform.
				*/
				for (int j = 0; j < BUBBLE_VERTICES_COUNT; ++j)
				{
					m_bubbleVerticesColors[j * COLOR_CHANNELS + R] = i->getColorR();
					m_bubbleVerticesColors[j * COLOR_CHANNELS + G] = i->getColorG();
					m_bubbleVerticesColors[j * COLOR_CHANNELS + B] = i->getColorB();
				}
				glColorPointer(COLOR_CHANNELS, GL_FLOAT, 0, m_bubbleVerticesColors);

				glPushMatrix();
				
					glTranslatef(i->getX(), i->getY(), 0.0f);
					glScalef(i->getRadius(), i->getRadius(), 1.0f);				
				
					glDrawArrays(GL_TRIANGLE_FAN, 0, BUBBLE_VERTICES_COUNT);

				glPopMatrix();
			}

			glEnableClientState(GL_COLOR_ARRAY);
			glDisableClientState(GL_VERTEX_ARRAY);

			//scores rendering

			glListBase(BASE_TEXT_LIST_INDEX); 
			glColor3f(1.0f, 1.0f, 1.0f);
			glRasterPos2f(SCORES_LEFT_OFFSET, m_viewHeight - SCORES_BOTTOM_OFFSET);

			::_snprintf_s(scoresLabelText, SCORES_LABEL_MAX_TEXT_LENGTH, "scores: %d", scores);

			glCallLists(::strlen(scoresLabelText), GL_UNSIGNED_BYTE, scoresLabelText);
		}
	}
}
