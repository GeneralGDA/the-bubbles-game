#pragma once

namespace game
{
	class Bubble
	{
	private:

		//do not use constants to allow compiler to generate "=" operator

		float m_speed;

		float m_x;
		float m_y;

		float m_radius;

		int m_scores;

		float m_r;
		float m_g;
		float m_b;

	public:

		Bubble(const float startX, const float startY, const float radius, const float speed, const int scores);

		void setColor(const unsigned char r, const unsigned char g, const unsigned char b);
		void updatePosition(const float seconds);

		const float getX() const;
		const float getY() const;
		const float getRadius() const;
		
		const int getScores() const;

		const float getColorR() const;
		const float getColorG() const;
		const float getColorB() const;

	};
}
