#include <limits>

#include "Bubble.h"

namespace game
{
	Bubble::Bubble(const float startX, const float startY, const float radius, const float speed, const int scores)
		:
		m_x(startX),
		m_y(startY),
		m_radius(radius),
		m_speed(speed),
		m_scores(scores),
		m_r(0.0f),
		m_g(0.0f),
		m_b(0.0f)
	{
	}

	void Bubble::setColor(const unsigned char r, const unsigned char g, const unsigned char b)
	{
		using std::numeric_limits;

		m_r = r / static_cast<float>(numeric_limits<unsigned char>::max());
		m_g = g / static_cast<float>(numeric_limits<unsigned char>::max());
		m_b = b / static_cast<float>(numeric_limits<unsigned char>::max());
	}

	void Bubble::updatePosition(const float seconds)
	{
		m_y += m_speed * seconds;
	}

	const float Bubble::getX() const
	{
		return m_x;
	}

	const float Bubble::getY() const
	{
		return m_y;
	}

	const float Bubble::getRadius() const
	{
		return m_radius;
	}

	const int Bubble::getScores() const
	{
		return m_scores;
	}

	const float Bubble::getColorR() const
	{
		return m_r;
	}

	const float Bubble::getColorG() const
	{
		return m_g;
	}

	const float Bubble::getColorB() const
	{
		return m_b;
	}

}
