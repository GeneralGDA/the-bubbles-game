#pragma once

#include <vector>

#include "Bubble.h"

namespace game
{
	
	class BubblesField
	{
	private:

		typedef std::vector<Bubble> BubblesVector;
		BubblesVector m_bubbles;

		float m_timeUntilNextBubble;

		const float m_width;
		const float m_height;

		int m_scores;

		BubblesField& operator=(const BubblesField&);
		BubblesField(const BubblesField&);

	public:

		BubblesField(const int width, const int height);

		void update(const float seconds);
		void handleClick(const int x, const int y);

		const std::vector<Bubble>& getBubbles() const;
		const int getScores() const;

	};

}
