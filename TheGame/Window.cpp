#include <cassert>
#include <iostream>

#include "Resource.h"
#include "Window.h"

namespace utils
{
	Window::Window(const int width, const int height, WNDPROC windowProcedure, HINSTANCE hInstance, int nCmdShow)
		:
		m_hInstance(hInstance)
	{
		assert(0 != windowProcedure && "\'windowProcedure\' argument can\'t be null");

		::LoadString(m_hInstance, IDS_APP_TITLE, m_title, MAX_RESOURCE_STRING_LENGTH);
		::LoadString(m_hInstance, IDC_THEGAME, m_windowClass, MAX_RESOURCE_STRING_LENGTH);

		RegisterClass(windowProcedure);
		CreateMyWindow(width, height);
		
		::ShowWindow(m_handle, nCmdShow);
		::UpdateWindow(m_handle);
	}

	Window::~Window()
	{
		if (0 == ::UnregisterClass(m_windowClass, m_hInstance))
		{
			std::cerr << "failed to unregister window class " << m_windowClass << std::endl;
		}
	}

	void Window::RegisterClass(WNDPROC windowProcedure)
	{
		WNDCLASSEX windowClass;

		windowClass.cbSize = sizeof(WNDCLASSEX);
		windowClass.style = CS_HREDRAW | CS_VREDRAW;
		windowClass.lpfnWndProc = windowProcedure;
		windowClass.cbClsExtra = 0;
		windowClass.cbWndExtra = 0;
		windowClass.hInstance = m_hInstance;
		windowClass.hIcon = LoadIcon(m_hInstance, MAKEINTRESOURCE(IDI_THEGAME));
		windowClass.hCursor = LoadCursor(NULL, IDC_ARROW);
		windowClass.hbrBackground = reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1);
		windowClass.lpszMenuName = 0;
		windowClass.lpszClassName = m_windowClass;
		windowClass.hIconSm = LoadIcon(m_hInstance, MAKEINTRESOURCE(IDI_SMALL));

		const ATOM resultCode = ::RegisterClassEx(&windowClass);
		
		if (0 == resultCode)
		{
			throw WindowCreationException(L"Failed to register window class");
		}
	}

	void Window::CreateMyWindow(int width, int height)
	{
		//window style: non-sizable, without "maximize" box
		const DWORD windowStyle = WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX;
		
		const int screenWidth = ::GetSystemMetrics(SM_CXSCREEN);
		const int screenHeight = ::GetSystemMetrics(SM_CYSCREEN);

		RECT actualWindowRect;
		//placing the window in the screen center
		actualWindowRect.left = (screenWidth - width) / 2;
		actualWindowRect.top = (screenHeight - height) / 2;
		actualWindowRect.right = actualWindowRect.left + width;
		actualWindowRect.bottom = actualWindowRect.top + height;

		if (0 == ::AdjustWindowRect(&actualWindowRect, windowStyle, FALSE))
		{
			std::cerr << "call to \'AdjustWindowRect\' has failed" << std::endl;
		}

		m_handle = ::CreateWindow(
			m_windowClass
			, m_title
			, windowStyle
			, actualWindowRect.left								//x
			, actualWindowRect.top								//y
			, actualWindowRect.right - actualWindowRect.left	//width
			, actualWindowRect.bottom - actualWindowRect.top	//height
			, 0
			, 0
			, m_hInstance
			, 0);

		if (0 == m_handle)
		{	
			::UnregisterClass(m_windowClass, m_hInstance);
			throw WindowCreationException(L"Failed to create window");
		}
	}

	HWND Window::getHandle() const
	{
		return m_handle;
	}

	WindowCreationException::WindowCreationException(const TCHAR* const what)
		:
		BaseException(what)
	{
	}
}
