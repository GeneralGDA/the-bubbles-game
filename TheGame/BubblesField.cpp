#include <algorithm>
#include <cassert>
#include <cstdlib>
#include <limits>

#include "BubblesField.h"

namespace game
{
	namespace
	{
		float random(const float from, const float to)
		{
			const int randomInt = rand();
			const float normalized = randomInt / static_cast<float>(RAND_MAX);
			
			return from + (to - from) * normalized;	
		}

		unsigned char random()
		{
			return rand() % std::numeric_limits<unsigned char>::max();
		}

		//all times are in seconds
		const float MIN_BUBBLE_GENERATION_TIME = 1.0f;
		const float MAX_BUBBLE_GENERATION_TIME = 2.0f;

		const float MIN_BUBBLE_RADIUS = 10.0f;
		const float MAX_BUBBLE_RADIUS = 50.0f;

		const float MIN_BUBBLE_SPEED = 50.0f;
		const float MAX_BUBBLE_SPEED = 200.0f;
		
		const float MIN_BUBBLE_SCORE = 1.0f;
		const float MAX_BUBBLE_SCORE = 100.0f;
		
		const float speedFromRadius(const float radius)
		{
			const float normalizer = (MAX_BUBBLE_RADIUS - radius) / (MAX_BUBBLE_RADIUS - MIN_BUBBLE_RADIUS);
			return MIN_BUBBLE_SPEED + normalizer * (MAX_BUBBLE_SPEED - MIN_BUBBLE_SPEED);
		}

		const int scoresFromRadius(const float radius)
		{
			const float normalizer = (MAX_BUBBLE_RADIUS - radius) / (MAX_BUBBLE_RADIUS - MIN_BUBBLE_RADIUS);
			const float exactScore = MIN_BUBBLE_SCORE + normalizer * (MAX_BUBBLE_SCORE - MIN_BUBBLE_SCORE);

			return static_cast<int>(exactScore + 0.5f);
		}

		class IsBubbleDead
		{
		private:

			float m_border;

		public:

			IsBubbleDead(const float yBorder)
				:
				m_border(yBorder)
			{
			}

			bool operator()(const Bubble& victim)
			{
				return victim.getY() > m_border + victim.getRadius();
			}
		};
	}

	BubblesField::BubblesField(const int width, const int height)
		:
		m_width(static_cast<float>(width)),
		m_height(static_cast<float>(height)),
		m_timeUntilNextBubble(0.0f),
		m_scores(0)
	{
		assert(0 < width && "\'width\' argument must be greater than zero");
		assert(0 < height && "\'height\' argument must be greater than zero");
	}

	void BubblesField::update(const float seconds)
	{
		assert(0 <= seconds && "\'seconds\' argument must be greater than or equal to zero");

		for (BubblesVector::iterator i = m_bubbles.begin(), end = m_bubbles.end(); i != end; ++i)
		{
			i->updatePosition(seconds);
		}

		//removing bubbles which fall too deep (lower window's bottom edge)
		m_bubbles.erase(std::remove_if(m_bubbles.begin(), m_bubbles.end(), IsBubbleDead(m_height)), m_bubbles.end());

		m_timeUntilNextBubble -= seconds;

		if (0 >= m_timeUntilNextBubble)
		{
			const float radius = random(MIN_BUBBLE_RADIUS, MAX_BUBBLE_RADIUS);
			const float speed = speedFromRadius(radius);
			const float y = -radius;
			const float x = random(radius, m_width-radius);
			const int scores = scoresFromRadius(radius);

			Bubble newBubble(x, y, radius, speed, scores);
			/*
			The only minus of such color generation schema - possibility of bubble with color same as backgroud.
			To escape such case pallete can be used.
			*/
			newBubble.setColor(random(), random(), random());

			m_bubbles.push_back(newBubble);

			m_timeUntilNextBubble += random(MIN_BUBBLE_GENERATION_TIME, MAX_BUBBLE_GENERATION_TIME);
		}
	}

	const std::vector<Bubble>& BubblesField::getBubbles() const
	{
		return m_bubbles;
	}

	const int BubblesField::getScores() const
	{
		return m_scores;
	}
	
	void BubblesField::handleClick(const int x, const int y)
	{	
		//searching for clicked bubble (in some "" case could be optimized with some spatial data structure like quad-tree)
		for (BubblesVector::iterator i = m_bubbles.begin(), end = m_bubbles.end(); i != end; ++i)
		{
			const float directionX = i->getX() - x;
			const float directionY = i->getY() - y;
			const float sqrDistance = directionX * directionX + directionY * directionY;
			const float sqrRadius = i->getRadius() * i->getRadius();

			if (sqrDistance <= sqrRadius)
			{
				m_scores += i->getScores();
				
				//removing algorithm can be optimized: we can move the last bubble on the place of the clicked
				m_bubbles.erase(i);

				//assume that we can't click two bubbles at the same time
				return;
			}
		}
	}
}
