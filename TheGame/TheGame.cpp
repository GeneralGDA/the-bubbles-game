#define WIN32_LEAN_AND_MEAN 
#include <windows.h>
#include <windowsx.h>
#include <cstdlib>
#include <ctime>

#include "Window.h"
#include "RenderingSurface.h"
#include "BubblesField.h"
#include "BubblesRender.h"

namespace
{
	const int GAME_FIELD_WIDTH = 800;
	const int GAME_FIELD_HEIGHT = 600;

	struct Game
	{
		game::BubblesField* bubblesField;
	};

	//the only way to pass some context into window procedure is a global variable :(
	Game theGame;
}

LRESULT CALLBACK WindowProcedure(HWND, UINT, WPARAM, LPARAM);

int APIENTRY _tWinMain(HINSTANCE hInstance, HINSTANCE, LPTSTR, int nCmdShow)
{
	::srand(::GetTickCount());

	try
	{
		utils::Window window(GAME_FIELD_WIDTH, GAME_FIELD_HEIGHT, WindowProcedure, hInstance, nCmdShow);
		utils::RenderingSurface surface(window.getHandle());

		game::BubblesField bubblesField(GAME_FIELD_WIDTH, GAME_FIELD_HEIGHT);
		game::render::BubblesRender render(surface.getHDC());

		theGame.bubblesField = &bubblesField;

		MSG message;     
		::ZeroMemory(&message, sizeof(message));

		bool firstFrame = true;
		std::clock_t previousTime = std::clock();
		while(WM_QUIT != message.message)
		{
			if(::PeekMessage(&message, 0, 0, 0, PM_REMOVE))
			{
				::TranslateMessage(&message);
				::DispatchMessage(&message);
			}
			else
			{
				//I do not use QueryPerfomanceCounter because of the issues with multi-core CPUs
				const std::clock_t currentTime = std::clock();
				const float frameLength = (currentTime - previousTime) / static_cast<float>(CLOCKS_PER_SEC);
				previousTime = currentTime;

				bubblesField.update(frameLength);
				
				surface.beginFrame();

					if (firstFrame)
					{
						render.prepareView(GAME_FIELD_WIDTH, GAME_FIELD_HEIGHT);
						firstFrame = false;
					}

					render.perform(bubblesField.getBubbles(), bubblesField.getScores());

				surface.endFrame();
			}
		}
	}
	catch(const utils::BaseException& exception)
	{
		::MessageBox(0, exception.what(), L"Error", MB_OK);
		return EXIT_FAILURE;
	}
	
	return EXIT_SUCCESS;
}

LRESULT CALLBACK WindowProcedure(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_LBUTTONDOWN:
	case WM_RBUTTONDOWN:
		{
		const int mouseX = GET_X_LPARAM(lParam);
		const int mouseY = GET_Y_LPARAM(lParam);
		theGame.bubblesField->handleClick(mouseX, mouseY);
		}
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}

	return 0;
}
